let resultsCheckOut = (array) => {
  let ret = true;
  array.forEach((item) => {
    if (!item.isWithinMisMatchTolerance) {
        ret = false;
        console.error('The images didn\'t match! The mismatch was ' + item.misMatchPercentage + '%.');
    }
  });

  return ret;
};

describe('The image should match the reference for the', function () {
  beforeEach(function () {
    browser.url('/');
    browser.setViewportSize({ width: 1280, height: 800 });
  });

  afterEach(function () {
     expect(resultsCheckOut(browser.checkViewport()))
        .toBe(true);
  });

  it('first test', function () {
    // Some actions to perform, such as
    // browser.moveToObject('CSSSelector', +x: number, +y: number)
    // browser.buttonDown(buttonNumber: number)
    // browser.leftClick('CSSSelector', +x: number, +y: number)
  });
});
