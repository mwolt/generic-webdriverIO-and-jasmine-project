var path = require('path');
var VisualRegressionCompare = require('wdio-visual-regression-service/compare');
function getScreenshotName(basePath) {
  return function (context) {
    var type = context.type;
    var testName = context.test.title;
    var browserVersion = parseInt(context.browser.version, 10);
    var browserName = context.browser.name;
    var browserWidth = context.meta.width;
    return path.join(basePath, `${testName}/${browserName}_v${browserVersion}.png`);
  };
}

exports.config = {
  specs: [
      './spec/*Spec.js',
  ],
  exclude: [],
  maxInstances: 10,
  capabilities: [{
      maxInstances: 5,
      browserName: 'firefox',
    }, {
      maxInstances: 5,
      browserName: 'chrome',
    }, {
      maxInstances: 5,
      browserName: 'internet explorer',
      version: '11.0',
    },
  ],
  sync: true,
  logLevel: 'silent',
  coloredLogs: true,
  bail: 0,
  screenshotPath: './errorShots/',
  baseUrl: 'http://www.google.com',
  waitforTimeout: 10000,
  connectionRetryTimeout: 90000,
  connectionRetryCount: 3,
  services: ['selenium-standalone', 'visual-regression'],

  visualRegression: {
    compare: new VisualRegressionCompare.LocalCompare({
      referenceName: getScreenshotName(path.join(process.cwd(), 'screenshots/reference')),
      screenshotName: getScreenshotName(path.join(process.cwd(), 'screenshots/screen')),
      diffName: getScreenshotName(path.join(process.cwd(), 'screenshots/diff')),
      misMatchTolerance: 0.01,
    }),
    viewportChangePause: 200,
  },
  framework: 'jasmine',
  reporters: ['spec'],
  jasmineNodeOpts: {
    defaultTimeoutInterval: 10000,
    expectationResultHandler: function (passed, assertion) {
      // can perform actions on failure of a spec.
    },
  },
};
