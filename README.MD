Requires the following npm modules globally:

    grunt-cli

May require the npm modules (I forgot):

    chromedriver
    edgedriver
    selenium-standalone (probably not)

For the rest it's just `npm install` and `npm test`/`grunt test`.

To alter the webpage address, edit [line 37 in the `wdio.conf.js` file](https://gitlab.com/mwolt/generic-webdriverIO-and-jasmine-project/blob/master/wdio.conf.js#L37) or alter the call to `browser.url()` to use a complete address in the spec.
To alter the misMatchTolerance, edit [wdio.conf.js](https://gitlab.com/mwolt/generic-webdriverIO-and-jasmine-project/blob/master/wdio.conf.js#L48).

If the screenshots/reference folder is empty, running the test will create the reference pictures. After that all screenshots will be compared to those shots.