module.exports = function (grunt) {
  // 1. All configuration goes here
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jasmine: {
      coverage: {
        src: ['src/scripts/!(DEVELOPMENT)*.js'],
        options: {
          specs: ['spec/*Spec.js'],
          helpers: 'spec/helpers/*.js',
          template: require('grunt-template-jasmine-istanbul'),
          templateOptions: {
            coverage: 'coverage/coverage.json',
            report: [
                    { type: 'html', options: { dir: 'coverage' } },
                    { type: 'text-summary' },
                ],

            // thresholds: {
            //   lines: 75,
            //   statements: 75,
            //   branches: 75,
            //   functions: 90,
            // },
          },
        },
      },
    },
    webdriver: {
      test: {
        configFile: './wdio.conf.js',
      },
    },
  });

  // 3. Where we tell Grunt we plan to use this plug-in.

  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.loadNpmTasks('grunt-webdriver');

  // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
  grunt.registerTask('test',
                      [
                        'webdriver',
                      ]);
};
